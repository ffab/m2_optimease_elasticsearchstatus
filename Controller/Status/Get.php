<?php

namespace Optimease\ElasticSearchStatus\Controller\Status;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Store\Model\ScopeInterface;

class Get implements HttpGetActionInterface
{
    /**
     * @var Context
     */
    private Context $context;
    /**
     * @var JsonFactory
     */
    protected JsonFactory $_resultJsonFactory;
    /**
     * @var Curl
     */
    protected Curl $_curl;
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $_scopeConfig;

    /**
     * @param Context $context
     * @param Curl $curl
     * @param ScopeConfigInterface $scopeConfig
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context              $context,
        Curl                 $curl,
        ScopeConfigInterface $scopeConfig,
        JsonFactory          $resultJsonFactory)
    {
        $this->_curl = $curl;
        $this->_scopeConfig = $scopeConfig;
        $this->_resultJsonFactory = $resultJsonFactory;
    }

    /**
     * Simplified Result status [down || unknown || status : green, yellow or red]
     * Documentation : https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-health.html
     * Use by calling this url : https://your-domain.com/elastic-search/status/get
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** Set default result **/
        $result = ['status' => 'down'];

        /**  Check if search Engine Elasticsearch x. is used **/
        $searchEngine = $this->_scopeConfig->getValue('catalog/search/engine',
            ScopeInterface::SCOPE_STORE);
        if (strpos($searchEngine, 'elasticsearch') === false) {
            $result = ['status' => 'search engine not used'];
        }

        /**  Format the local url to be called by curl **/
        $hostname = $this->_scopeConfig->getValue('catalog/search/' . $searchEngine . '_server_hostname',
                ScopeInterface::SCOPE_STORE) ?? 'localhost';
        $port = $this->_scopeConfig->getValue('catalog/search/' . $searchEngine . '_server_port',
                ScopeInterface::SCOPE_STORE) ?? '9200';
        $url = 'http://' . $hostname . ':' . $port . '/_cluster/health?pretty';

        /**  make the status call and simplify answer **/
        try {
            $this->_curl->get($url);
        } catch (\Exception $e) {
            $result['status'] = 'down';
        }

        if ($this->_curl->getStatus() == 200) {
            $response = $this->_curl->getBody();
            $response = json_decode($response, true);
            $result['status'] = array_key_exists('status', $response) ? $response['status'] : 'unknown';
        }

        /** and return json with the according result*/
        $json = $this->_resultJsonFactory->create();
        return $json->setData($result);
    }

}
