## Optimease_ElasticSearchStatus
![](https://img.shields.io/static/v1?label=Version&message=v1.0.2&color=green) ![](https://img.shields.io/static/v1?label=Release%20Date&message=2021/08&color=blue) ![](https://img.shields.io/static/v1?label=Scope&message=Front)

**Module that allows to get simplified status of Elasticsearch for Magento 2.4.x by calling an url**

#### Table Of Contents
1. [Requirements](#module-requirements)
2. [Installation](#module-installation)
3. [Usage](#module-operating)
4. [Revision](#module-revision)

#### Requirements <a name="module-requirements"></a>
- Php 7.4
- Magento 2.4.x (Open Source)

#### Installation <a name="module-installation"></a>
- Upload Optimease/ElasticSearchStatus files.
- run ```php bin/magento setup:upgrade```

#### Usage <a name="module-operating"></a>
- Format a link with your domain `https://your-domain.com/elastic-search/status/get`
- Paste it in your browser or any other client.
- In return, you will get a very simple json : ```{"status":"value"}```
- Where **value** can be **colors**, like:
  - **green** : all good
  - **yellow** : good but not perfect
  - **red** : error
  - please refer to the [original documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-health.html) for that part
- And some additional values has been added
  - **down** : elastic search engine is probably down
  - **unknown** : you don't use elastic search anyway

#### Revision <a name="module-revision"></a>
- 1.0.2 - 2021-08-27 - Catch exception on curl connection
- 1.0.1 - 2021-08-18 - Small non-blocking fix in composer.json
- 1.0.0 - 2021-08-17 - First release
